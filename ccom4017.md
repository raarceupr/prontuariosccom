<center>
Universidad de Puerto Rico
Recinto de Río Piedras<br> Facultad de Ciencias Naturales<br>
Departamento de Ciencia de Cómputos
</center>

---

### Curso: Sistemas Operativos

### Código:	CCOM 4017

### Número de créditos/horas: 3 horas, 3 créditos

### Prerrequisitos: CCOM 3034, CCOM 4086

---

### Descripción:

CCOM 4017. Sistemas Operativos. Tres créditos. Tres horas de conferencia a la semana. Requisito: CCOM 3034.

Conjunto de instrucciones. Entradas y salidas. Esquemas direccionales,  Implementación de procedimientos,  Manejo de memorias.  Estructura y evaluación de sistemas.  Procedimientos de recuperación.  Aplicaciones.


### Objetivos:

1. Estudiar la coordinación y comunicación de procesos y los problemas asociados.

2. Identificar las funciones que constituyen el kernel de un sistema operativo.

3. Estudiar las características principales de las rutinas de Entrada/ Salida y la organización de un sistema de archivos.

4. El estudiante podrá contribuir de forma electiva a la inclusión de compañeros estudiantes con impedimentos en el salón de clase.

5. El estudiante podrá, al trabajar en equipo, hacer los acomodos 	necesarios para incluir compañeros estudiantes con impedimento.

### Sílabo:

1. Introducción:
a) Concepto de sistema operativo
b) Evolución histórica de los sistemas operativos
c) Características y funciones del sistema operativo
d) Funciones de supervisión
e) Concepto de proceso y estado


2. Administración y Manejo de la Memoria:
a) Jerarquización de la memoria
b) Organización de la memoria principal
c) Técnicas de manejo de la jerarquía
d) Compartición  y protección de procesos

3. Memoria Virtual Paginada:
a) Técnicas de ubicación
b) Técnicas de carga
c) Reemplazo para participaciones fijas
d) Reemplazo para participaciones variables

4. Memoria Virtual Segmentada:
a) Técnicas de ubicación y carga
b) Técnicas de reemplazo
c) Memoria virtual segmentada y virtualizada

5. Memoria Secundaria:
a) Arquitectura de la unidad del disco
b) Organización de la memoria secundaria

6. Comunicación y sincronización de procesos:
a) Ejecución concurrente de procesos
b) Grafos de precedencia y condiciones de Bernstein
c) Especificación de la concurrencia
d) El problema de la selección crítica
e) Mecanismo de sincronización

7. Interbloqueo:
a) Grafos de la asignación de recursos
b) Prevención. Evitación.
c) Detección y Recuperación

8. El Kernel:
a) Características y funciones
b) Manejador de interrupciones
c) Dispatcher
d) Primitivas de la comunicación y sincronización
e) Control del tiempo

9. Entrada/ Salida:
a) Características de periféricos
b) Rutinas de Entrada/Salida
c) Controladores e interrupciones
d) Buffering. Spooling.

10. Sistemas de archivos:
a) Directorios, búsqueda y acceso
b) Organización del almacenamiento
c) Apertura y cierre
d) Uso compartido y seguridad. Integridad

11. Caso de Estudio: Sistema Operativo Unix:
a) Origen y evolución
b) El kernel.  Manejo de procesos y memoria
c) Entrada/ Salida
d) Sistemas de archivos

### Estrategias instruccionales:
Discusiones, trabajos colaborativos, estudio independiente, lecturas de artículos  de revistas especializadas, entre otros.


### Recursos mínimos disponibles o requeridos:
Salón de computadoras con servicio de Internet

### Estrategias de evaluación:
|  |  Porcentaje |
|------|--------|
| Asignaciones (cada 10 días) | 15% |
| 2 exámenes parciales | 30% (15% cada uno) |
| Examen final | 15% |
| 1 proyecto en equipo | 15% |
| Reseña detallada de un articulo | 15% |
| Asistencia y participación en clase | 10% |
| Total	| 100% |


### Sistema de calificación:
Se utilizará un sistema de calificación en la escala de la A-F

### Derechos de Estudiantes con Impedimentos:
Los estudiantes que reciban servicios de Rehabilitación Vocacional deben 	comunicarse con el (la) profesor(a) al inicio del semestre para planificar el 	acomodo razonable y equipo asistido necesario conforme a las recomendaciones 	de la Oficina de Asuntos para las Personas con Impedimentos (OAPI) de Decanto 	de Estudiantes.  También aquellos estudiantes con necesidades especiales que 	requieran de algún tipo de asistencia o acomodo deben comunicarse con el (la) 	profesor(a).

### Bibliografía:

1. Deitel, H.M., Introducción a los Sistemas Operativos, Adisson-Wesley.
1. Silverchatz, Galvin y Gagne. Applied Operating System Concepts, Wiley.
1. Tanenbaum, A.S.Modern Operating Systems, Prentice- Hall.
