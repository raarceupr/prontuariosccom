<center>
Universidad de Puerto Rico
Recinto de Río Piedras<br> Facultad de Ciencias Naturales<br>
Departamento de Ciencia de Cómputos
</center>


----

### Título: Introducción a la Ingeniería de Software

#### Codificación: CCOM 4030

#### Número de horas/crédito: 45 horas, 3 créditos

#### Prerequisitos, corequisitos y otros requerimientos	CCOM 4029

----

### Descripción del curso:

Este curso está diseñado para estudiantes que cursan su tercer o cuarto año en el programa de Ciencia de Cómputos. Provee una visión completa de los principios básicos y conceptos de la ingeniería de software. Discute elementos del ciclo de vida del software, análisis de requerimientos, implementación, verificación y validación, así como asuntos éticos relacionados al proceso de desarrollo de software. Además, a través de un proyecto en grupo obtienen experiencia que le permite al estudiante asumir posiciones como diseñadores y desarrolladores de software.

This course is designed for students who attend their third or fourth year in the Computer Science program. Provides an overview of the basic principles and concepts of software engineering. Discusses elements of the software life cycle, requirements analysis, implementation, verification and validation as well as ethical issues related to software development process. In addition, through a group project the students will gain experience that enables them to assume positions as designers and developers of software.


### Objetivos del curso

Al final del curso el estudiante:

1.	Conocerá los fundamentos del ciclo de vida del software

2.	Analizará los requerimientos de un proyecto de desarrollo de software

3.	Implementará un proyecto de desarrollo de software en equipo

4.	Verificará y validará un proyecto de desarrollo de software.

5.	Manejará los asuntos éticos fundamentales del proceso de desarrollo de software

6.	Apreciará la importancia de la Ingeniería de Software para el desarrollo de un proyecto de software

7.	Adquirirá una actitud positiva hacia el desarrollo de un proyecto de software en equipo

8.	Adquirirá destrezas de diseñador y desarrollador de software.



### Bosquejo de contenido y distribución del tiempo


| |  Tema |  Horas |
|-----|----------------------------------------------|--------------|
| 1.  | Introducción a la Ingeniería de Software     | (4.5 horas)  |
| 2.  | Confiabilidad                                | (1.5 horas)  |
| 3.  | Procesos del Software                        | (3 horas)    |
| 4.  | Gestión de Proyectos                         | (3 horas)    |
| 5.  | Aspectos éticos de la Ingeniería de Software |  (4.5 horas) |
| 6.  | Requisitos del Software                      | (4.5 horas)  |
| 7.  | Modelos                                      | (4.5 horas)  |
| 8.  | Arquitectura del Sistema y Diseño            |  (4.5 horas) |
| 9.  | Diseño de Interfase al Usuario               | (1.5 horas)  |
| 10. | Arquitectura de Sistemas Distribuidos        | (1.5 horas)  |
| 11. | Desarrollo Iterativo de Software             | (1.5 horas)  |
| 12. | Ingeniería de Software Basada en Componentes | (3 horas)    |
| 13. | Verificación y Validación                    | (4.5 horas)  |
| 14. | Aceptación y entrega                         | (3 horas)    |
| | Total | 45 horas |


### Estrategias instruccionales

Discusiones, proyecto en equipo, trabajos colaborativos, estudio independiente, lecturas de artículos  de revistas especializadas, entre otros.



### Recursos mínimos disponibles o requeridos

Salón de computadoras con servicio de Internet

### Estrategias de evaluación

|  |  Porcentaje |
|-------------------------------------|------|
| Asignaciones                        | 15%  |
| Examen parcial                      | 15%  |
| Proyecto en equipo                  | 45%  |
| Reseña detallada de un articulo     | 15%  |
| Asistencia y participación en clase | 10%  |
| Total | 100%|

Evaluación diferenciada a estudiantes con necesidades especiales.

### Sistema de calificación
Se utilizará un sistema de calificación en la escala de la A - ­F

### Derechos de Estudiantes con Impedimentos
La Universidad de Puerto Rico cumple con todas las leyes federales y estatales, y reglamentos concernientes a discriminación, incluyendo “The American Dissabilities Act” (Ley ADA) y la Ley 51 del Estado Libre Asociado de Puerto Rico. Los estudiantes que reciban servicios de rehabilitación vocacional deben comunicarse con el profesor al inicio del semestre para planificar el acomodo razonable y equipo de asistencia necesario conforme con las recomendaciones de la Oficina de Asuntos para las Personas con Impedimentos (OAPI) del Decanato de Estudiantes.

### Libro de texto sugerido

Ian Sommerville, Software Engineering, Eighth Edition, Addison-Wesley, 2006.

### Bibliografía

1. Roger Pressman, Software Engineering: A Practitioner's Approach, Seventh Edition, McGraw-Hill, 2009.

1. Shari Lawrence Pfleeger, Joanne M. Atlee, Joanne Atlee, Software Engineering Theory and Practice, third edition.  Prentice- Hall, 2005.

1. Grady Booch, James Rumbaugh, Ivar Jacobson, The Unified Modeling Language User Guide, Second Edition, Addison-Wesley, 2005.

1. Andrew Hunt and David Thomas The Pragmatic Programmer: From Journeyman to Master, 1999.

1. Frederick P. Brooks, Jr., The Mythical Man Month. Essays on Software Engineering, Addison-Wesley, 1995.

1. Tsui Karam, Essentials of Software Engineering, Second Edition, Jones and Bartlett, 2010

### Referencias electrónicas:

1. Software Engineering Code of  Ethics. http://www.acm.org/about/se-code (verificada el 11 de febrero del 2010)

1. Guide to the Software Engineering Body of Knowledge. http://www.swebok.org/ (verificada el 11 de febrero del 2010)

1. ACM Special Interest Group in Software Engineering. http://www.sigsoft.org/ (verificada el 11 de febrero del 2010)


### Revistas del área

1. IEEE Transactions on Software Engineering. http://www.computer.org/portal/web/tse/

1. IEEE Software. http://www.computer.org/portal/web/software/home

1. Software Engineering Notes. http://www.sigsoft.org/SEN/
