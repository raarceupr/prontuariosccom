<center>
Universidad de Puerto Rico
Recinto de Río Piedras<br> Facultad de Ciencias Naturales<br>
Departamento de Ciencia de Cómputos
</center>

---

### Titulo: Estudio Independiente en Ciencia de Cómputos

### Codificación: CCOM 4996

### Número de horas/crédito: 15 horas contacto, 1 a 3 créditos

### Prerrequisitos y corequisitos: Autorización del profesor

---

### Descripción del curso:

Este curso ofrece a los estudiantes la oportunidad de hacer un estudio independiente y aprender sobre temas relacionados a la ciencia de cómputos, guiado por un profesor. Los temas se escogerán de acuerdo al interés del estudiante y el profesor. Al finalizar el estudio, el estudiante redactará un ensayo o informe técnico sobre el tema estudiado. El curso podrá repetirse bajo un tema de estudio diferente, hasta un máximo de 3 créditos.

This course gives the students the opportunity to conduct an independent study and to learn about topics related to computer science, guided by a professor. The topics will be selected according to the interest of the student and the professor. At the end, the student will write an essay or technical report about the topic studied. The course can be taken more than once if the topic is difference, with a maximum of 3 credits. 



### Objetivos del curso:
Al finalizar el curso el estudiante habrá:

1. aprendido sobre un tema relacionado a la ciencia de cómputos.

1. reforzado su interés en la ciencia de cómputos

1. desarrollado su capacidad para el estudio independiente.

1. escrito un ensayo o informe técnico sobre el tema estudiado.

1. alcanzado otros objetivos específicos determinados por el profesor que dirija el estudio independiente.



### Bosquejo de contenido y distribución del tiempo:

Habrá 15 horas de reunión entre los estudiantes y el profesor (1 hora a la semana) para discutir el progreso del estudio. El bosquejo de contenido y distribución del tiempo serán determinado por el profesor que dirija el estudio independiente. 

### Estrategias instruccionales

Discusiones semanales. Otras estrategias serán determinadas por el profesor que dirija el estudio independiente.


### Recursos mínimos disponibles o requeridos

Serán determinados por el profesor que dirija el estudio independiente.

### Estrategias de evaluación:

El estudiante será evaluado tomando en consideración su progreso las discusiones semanales con el profesor y utilizando el ensayo o informe técnico que se entrega al final del curso. Otros métodos de evaluación serán determinados por el profesor que dirija el estudio independiente.

Evaluación diferenciada a estudiantes con necesidades especiales.

### Sistema de calificación:

El curso tendrá calificación de A, B, C, D, F.

### Derechos de Estudiantes con Impedimentos:
La Universidad de Puerto Rico cumple con todas las leyes federales y estatales, y reglamentos concernientes a discriminación, incluyendo “The American Dissabilities Act” (Ley ADA) y la Ley 51 del Estado Libre Asociado de Puerto Rico. Los estudiantes que reciban servicios de rehabilitación vocacional deben comunicarse con el profesor al inicio del semestre para planificar el acomodo razonable y equipo de asistencia necesario conforme con las recomendaciones de la Oficina de Asuntos para las Personas con Impedimentos (OAPI) del Decanato de Estudiantes.

### Bibliografía sugerida:

Las fuentes bibliográficas serán determinadas por el profesor que dicte el curso, conforme al tema de estudio. Algunas fichas generales son:

1.  N. Domínguez, *Módulo Instruccional: Cómo redactar y preparar un ensayo*, 
http://ccom.uprrp.edu/resource_media.php?aid=14

2.  N. Domínguez, *Módulo Instruccional: Uso de las bases de datos*,
http://ccom.uprrp.edu/resources_info.php?aid=12

3.  L. Pagán, *Como definir un tema de investigación, Módulo Instruccional*,
http://www.youtube.com/bibliotecaeea#p/a/u/0/v9-xgUK7x0I 

4.  I. Rubio, *Una introducción breve a Latex*, folleto,  2011, 
http://ccom.uprrp.edu/actividades/IntroLatex.pdf

5.  L. Torres, *Asistencia Tecnológica: derecho de todos*, Editorial Isla Negra, 2002.

6.  Otras fuentes bibliográficas serán determinadas por el profesor que dirija el estudio independiente.


### Referencias Electrónicas:

Las referencias electrónicas serán determinadas por el profesor que dirija el proyecto, y variarán conforme al proyecto a realizarse.
